'use strict'

/**
* @namespace com.sample.libs
* @auther Graymatrix soluations PVT LTD
* @version 1.0.0
*/

/**
* @class base_lib
* @classdesc library Base Class
* @memberof com.sample.libs
*/

const path = require("path");

/**
 * Export Base Library Class
 * @memberof com.sample.libs
 * @module base_lib
 * @see com.sample.libs.base_lib
 */

module.exports.base_lib = class base_lib{
	
	constructor(){
	}
	
};
