function stickyFooter() {
    var $footerHeight = $('#siteFooter').innerHeight();
    $('.page-wrapper').css('padding-bottom', $footerHeight);
    return $footerHeight;
}

function shiftHeroSlider() {
    var $siteHeight = $('#siteHeader').innerHeight();
    if($('.masthead').length) {
        $('.masthead').css('margin-top', '-' + $siteHeight + 'px');
    } else if($('.pb-wrapper').length) {
        $('.pb-wrapper').css('margin-top', '-' + $siteHeight + 'px');
    }
    return $siteHeight;
}

function toggleMobiMenu() {
    $('.menu-icon__m').on('click', function() {
        $('body').addClass('open-mobi-nav');
    });
    $('body').on('keyup', function(e) {
        if(e.keyCode === 27) {
            if($('.open-mobi-nav').length) {
                $('body').removeClass('open-mobi-nav');
            }
        }
    })
    $('.overlay-mobi-modal').on('click', function() {
        if($('.open-mobi-nav').length) {
            $('body').removeClass('open-mobi-nav');
        }
    })
}
function postEqHeight() {
    return $('.single-post .post-meta').matchHeight();
}

// function gridTwoPortrait() {
//     if($(window).innerWidth() < 787) {
//         // var bg = $('.bg-overlay');
//         if(bg.length) {
//             bg.each(function(){
//                 // var bgUrl = $(this).css('background-image');
//                 // bgUrl = bgUrl.replace('url(','').replace(')','').replace(/\"/gi, "");
                
//                     // $(this).parent().append('<img src="'+ bgUrl +'" class="bg-image visible-sm visible-xs"/>');
                
//             });
//         }
//     } 
// }