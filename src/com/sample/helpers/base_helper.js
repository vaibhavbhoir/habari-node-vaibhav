'use strict'

/**
* @namespace com.sample.helpers
* @auther Graymatrix soluations PVT LTD
* @version 1.0.0
*/

/**
* @class base_helper
* @classdesc Application Base Model
* @memberof com.sample.helpers
*/

var Joi = require('joi');

/**
 * Export Base Helper Class
 * @memberof com.sample.helpers
 * @module base_helper
 * @see com.sample.helpers.base_helper
 */

module.exports.base_helper = class base_helper{
	constructor(){
	}
};
