'use strict'

/**
* @namespace com.sample.commands
* @auther Graymatrix soluations PVT LTD
* @version 1.0.0
*/

/**
* @class base_command
* @classdesc Application Base Controller
* @memberof com.sample.commands
*/

/**
 * Export Base Controller Class
 * @memberof com.sample.commands
 * @module base_command
 * @see com.sample.commands.base_controller
 */

module.exports.base_command = class base_command{
	
	constructor(){
	}
};
