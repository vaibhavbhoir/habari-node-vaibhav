'use strict'

/**
* @class web_routes
* @extends com.sample.routes.base_routes
* @classdesc Application global app routes
* @memberof com.sample.routes
*/

const baseRoute = require("./base_routes.js").base_routes;
const path		= require("path");
	
/**
 * Export App Routes Class
 * @memberof com.sample.routes
 * @module api_routes
 * @see com.sample.routes.web_routes
 */

module.exports.web_routes = class web_routes extends baseRoute{
	
	constructor(app){
		super(app,__filename);
	}
	
	/**
	* @summary create static URLs
	* @public
	* @memberof com.sample.routes.web_routes
	* @function load
	* @override
	*/
	
	load(){
		this.app.get("/",this.controller.beforeLoad,this.controller.getHome);
		this.app.get("/about",this.controller.beforeLoad,this.controller.getAbout);
		this.app.get("/contact",this.controller.beforeLoad,this.controller.getContact);

		this.app.get("/power-of-connectivity",this.controller.beforeLoad,this.controller.getPowerOfConnectivity);
		this.app.get("/year-of-return",this.controller.beforeLoad,this.controller.getYearOfReturn);
		this.app.get("/five-books",this.controller.beforeLoad,this.controller.getFiveBooks);
		this.app.get("/end-of-year",this.controller.beforeLoad,this.controller.getEndOfYear);
		this.app.get("/future-of-work",this.controller.beforeLoad,this.controller.getFutureOfWork);
		this.app.get("/lifeat-absa",this.controller.beforeLoad,this.controller.getLifeAtAbsa);






	}
};
